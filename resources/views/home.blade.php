<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Paint Nite</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <!-- Styles -->
        <style>
            body {
                overflow: hidden;
                margin: 0px;
            }
            .container {
                position: relative;
                width: 100%;
            }
            .container .social_btns {
                position: absolute;
                top: calc(50% - 20px);
                left: calc(50% - 135px);
            }
            .container .btn {
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                background-color: #f04930;
                color: white;
                font-size: 20px;
                padding: 12px 24px;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                text-align: center;
                text-decoration: none;
            }

            .container .btn:hover {
                background-color: #ad1e08;
            }
        </style>
    </head>
    <body>
        {!! Toastr::render() !!}
        <div class="container">
            <img src ="assets/images/homepage-paint.jpg" style="width:100%; max-height: 100%;" />
            <div class="social_btns">
                <a href="redirect/facebook" class="btn">Facebook</a>
                <a href="redirect/instagram" class="btn">Instagram</a>
            </div>
        </div>
    </body>
</html>
