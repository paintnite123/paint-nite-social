@extends('layouts.master')
@section('title') View All Posts @endsection
@section('content')
<div class="img_container" style="background-image: url(assets/images/homepage-paint.jpg)">
    <!-- <div class="social_btns">
        <a href="redirect/facebook" class="btn">Facebook</a>
        <a href="redirect/instagram" class="btn">Instagram</a>
    </div> -->
</div>
<div class="container table_section">
    <h1>All Posts</h1>
    <div class="row table_row">
            <table id="users" class="table table-striped table-bordered table-sm" cellspacing="0" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 30px !important;">Sr.No</th>
                        <th>Page Name</th>
                        <th>Post Text</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($posts as $post)
                    <tr>
                        <td style="width: 50px; text-align: center;"><?php echo $i; ?></td>
                        <td>{{ $post->page->name }}</td>
                        <td>{{ $post->text }}</td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection
