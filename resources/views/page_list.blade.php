@extends('layouts.master')
@section('title') View All Pages @endsection
@section('content')
<div class="img_container" style="background-image: url(assets/images/homepage-paint.jpg)">
    <!-- <div class="social_btns">
        <a href="redirect/facebook" class="btn">Facebook</a>
        <a href="redirect/instagram" class="btn">Instagram</a>
    </div> -->
</div>
<div class="container table_section">
    <h1>All Pages</h1>
    <div class="row table_row">
            <table id="users" class="table table-striped table-bordered table-sm" cellspacing="0" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 30px !important;">Sr.No</th>
                        <th>User Name</th>
                        <th>Page Name</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($pages as $page)
                    <tr>
                        <td style="width: 50px; text-align: center;"><?php echo $i; ?></td>
                        <td>{{ $page->user->name }}</td>
                        <td>{{ $page->name }}</td>
                        <td>{{ $page->category }}</td>
                        <td>{{ $page->description }}</td>
                        <td>
                            <a href="{{route('post',['id'=>$page->id])}}">
                                <i class="fa fa-plus icon_custom_style" title="Add New Post"></i>
                            </a>
                            <a href="<?php echo 'https://www.facebook.com/'.str_replace(' ', '-', $page->name).'-'.$page->fb_page_id.'/settings/?tab=admin_roles&ref=page_edit'; ?>" target="_blank">
                                <i class="fa fa-eye icon_custom_style" title="View On Facebook"></i>
                            </a>
                            <!-- <a href="{{route('post_list')}}"> -->
                                <!-- <i class="fa fa-eye icon_custom_style" title="View All Posts"></i> -->
                            <!-- </a> -->
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection
