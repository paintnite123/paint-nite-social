@extends('layouts.master')
@section('title') Create Post @endsection
@section('content')
<div class="container">
	<h1>Create Post </h1>
    <form method="POST" action="{{ route('store_post') }}" enctype="multipart/form-data">
    	@csrf
        <input type="hidden" name="page_id" value="{{ $data->id }}">
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label ">Post Title</label>
                <input type="text" name="title" class="form-control" autofocus />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label ">Post Message</label>
                <textarea name="text_message" rows="4" class="form-control"></textarea>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label">Picture/Video</label>
                <input type="file" name="media_file" id="media_file" accept="image/x-png,image/gif,image/jpeg,video/mp4" onchange="Filevalidation()" />
            </div>
        </div>
        <br>
        <br>
        <div class="row">
        	<div class="col-md-6">
        		<span class="button-container">
        			<button class="submit-state-button btn-empty btn-secondary" id="submit_post" disabled>
            			<span class="spinner"></span>
            			<span>Submit</span>
            		</button>
            	</span>
        	</div>
        </div>
    </form>
</div>
<br><br>
@endsection
