@extends('layouts.master')
@section('title') Paint Nite @endsection
@section('content')
<div class="img_container" style="background-image: url(assets/images/homepage-paint.jpg)">
    <div class="social_btns">
        <a href="redirect/facebook" class="btn">Login With Facebook</a>
        <!-- <a href="redirect/instagram" class="btn">Instagram</a> -->
    </div>
</div>
<div class="container table_section">
    <div class="row table_row">
            <table id="users" class="table table-striped table-bordered table-sm" cellspacing="0" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 30px !important;">Sr.No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Facebook Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach ($users as $user)
                    <tr>
                        <td style="width: 50px; text-align: center;"><?php echo $i; ?></td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->fb_name }}</td>
                        <td style="text-align: center;">
                            <a href="{{route('page',['id'=>$user->id])}}">
                                <i class="fa fa-plus icon_custom_style" title="Add New Page"></i>
                            </a>
                            <a href="{{route('page_list')}}">
                                <i class="fa fa-eye icon_custom_style" title="View All Pages"></i>
                            </a>
                        </td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection
