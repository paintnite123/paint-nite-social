<!DOCTYPE html>
<html lang="en">
    <head>

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>@yield('title')</title>

        <link rel="stylesheet" href="{{asset('css/custom.css')}}">
        <link rel="stylesheet" href="{{asset('css/static.css')}}">        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    </head>
    <body>
        <div class="header-container">
            <header class="header-nav-component desktop">
                <div class="right-aligned-components">
                    <div>
                        <div class="header-container__lpPe_component">
                            <ul class="header-container__lpPe_outerList">
                                <li class="header-container__lpPe_outerList_item">
                                    <div class="standard-link">
                                        
                                    </div>
                                </li>
                                <li class="header-container__lpPe_outerList_item">
                                    <div class="standard-link">
                                        
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="header-nav-component__content">
                    <div class="underlay closed"></div>
                    <div class="logo-wrapper">
                        <a href="{{route('home')}}">
                            <img class="logo-wrapper__logo" src="{{asset('assets/images/paintnite-logo.svg')}}" height="45" alt="Paint Nite Logo">
                        </a>
                    </div>
                </div>
            </header>
        </div>
        {!! Toastr::render() !!}
        @yield('content')
        <div class="footer">
            <p><span>© 2019 Paint Nite, LLC d/b/a Yaymaker. All Rights Reserved.</span></p>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#users').DataTable();
            });
            Filevalidation = () => { 
                var fi = document.getElementById('media_file'); 
                // Check if any file is selected. 
                if (fi.files.length > 0) {
                    for (var i = 0; i <= fi.files.length - 1; i++) {
          
                        var fsize = fi.files.item(i).size; 
                        var file = Math.round((fsize / 1024)); 
                        // The size of the file. 
                        if (file >= 2048) { 
                            alert("File size is large, Please try with a small file!"); 
                        } else { 
                            $('#submit_post').removeAttr("disabled");
                        } 
                    } 
                } 
            }
        </script>
    </body>

</html>

