@extends('layouts.master')
@section('title') View All Sync Users @endsection
@section('content')
<div class="container">
	<h1>Create Page On Facebook</h1>
    <form method="POST" action="{{ route('store_page') }}" enctype="multipart/form-data">
    	@csrf
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="row">
            <div class="col-md-6">
                <label class="form-label ">Page Name</label>
                <input type="text" name="page_name" class="form-control" required autofocus />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label ">Category</label>
                <select class="form-control" name="category" required>
                    <?php
                    foreach ($data as $result) {
                        if (isset($result['fb_page_categories'])) {
                            foreach ($result['fb_page_categories'] as $category) {
                                if (isset($category['fb_page_categories'])) {
                                    foreach ($category['fb_page_categories'] as $cat) {
                                        ?>
                                        <!-- <option value="<?php // echo $cat['api_enum']; ?>"><?php // echo $cat['name']; ?></option> -->
                                        <option value="PERSONAL_BLOG"><?php echo $cat['name']; ?></option>
                                        <?php
                                    }
                                }
                                else{
                                    ?>
                                    <option value="PERSONAL_BLOG"><?php echo $category['name']; ?></option>
                                    <!-- <option value="<?php // echo $category['api_enum']; ?>"><?php // echo $category['name']; ?></option> -->
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label">Description</label>
                <input type="text" name="description" class="form-control" required />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label">Picture</label>
                <input type="file" name="picture" accept="image/x-png,image/gif,image/jpeg" required />
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <label class="form-label">Cover Photo</label>
                <input type="file" name="cover_photo" accept="image/x-png,image/gif,image/jpeg" required />
            </div>
        </div>
        <br>
        <div class="row">
        	<div class="col-md-6">
        		<span class="button-container">
        			<button class="submit-state-button btn-empty btn-secondary">
            			<span class="spinner"></span>
            			<span>Submit</span>
            		</button>
            	</span>
        	</div>
        </div>
    </form>
</div>
<br><br>
@endsection
