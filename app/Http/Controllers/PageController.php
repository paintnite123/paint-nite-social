<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FbPage;
use Kamaln7\Toastr\Facades\Toastr;
use Validator;
use Session;
use Exception;
use App\Post;

class PageController extends Controller {
    
    public function __construct() {
        
    }

    public function index($id) {
    	$user = User::find($id);
    	$fb = new \Facebook\Facebook([
                    'app_id' => config('app.fb_app_id'),
                    'app_secret' => config('app.fb_app_secret'),	  
                    'default_graph_version' => 'v5.0'
		]);

        try {
                $response = $fb->get(
                    '/fb_page_categories',
                    $user->fb_token
                );
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
		
        $res = $response->getDecodedBody();
        $data = $res['data'];
        return view('create_page',compact('user', 'data'));
    }

    public function store(Request $request) {
    	$user = User::find($request->id);
		
        if($request->hasfile('picture') && $request->hasfile('cover_photo')) {
            $picture_name= time() . 'aa' . $request->file('picture')->getClientOriginalName();
            $cover_photo_name= time() . 'bb' . $request->file('cover_photo')->getClientOriginalName();
            $request->file('picture')->move(public_path('fb_pictures'), $picture_name);
            $request->file('cover_photo')->move(public_path('fb_cover_photos'), $cover_photo_name);
        }
        $data = new FbPage;
        $data->user_id = $request->id;
        $data->fb_user_id = $user->fb_id;
        $data->name = 'Paint Nite '.$request->page_name;
        $data->category = $request->category;
        $data->description = $request->description;
        $data->picture = $picture_name;
        $data->cover_photo = $cover_photo_name;
        $data->save();

        return redirect()->route('create_page', ['id' => $data->id, 'user_id' => $request->id]);
    }

    public function create($id, $user_id) {
        $user = User::find($user_id);
        $fb_user_id = $user->fb_id;
        $fb_user_token = $user->fb_token;
    	$pic_url = config('app.fb_pictures_base_path');
        $cover_url = config('app.fb_cover_photos_base_path');

        $last_id = $id;
        $fb = FbPage::find($last_id);
        $name = $fb->name;
        $category = $fb->category;
        $description = $fb->description;
        $picture = $pic_url . $fb->picture;
        $cover_photo = $cover_url . $fb->cover_photo;

        try {
            $fb = new \Facebook\Facebook([
                    'app_id' => config('app.fb_app_id'),
                    'app_secret' => config('app.fb_app_secret'),	  
                    'default_graph_version' => 'v2.10'
                ]);


            $data = array (
                    'name' => $name,
                    'category_enum' => $category,
                    'about' => $description,
                    'picture' => $picture,
                    'cover_photo' => '{"url":"'.$cover_photo.'"}'
                );
		   
            $response = $fb->post(
                    '/'.$fb_user_id.'/accounts',
                    $data,
                    $fb_user_token
                );

            $graphNode = $response->getGraphNode();
            $page_data = FbPage::find($last_id);
            $page_data->fb_page_id = $graphNode['id'];
            $page_data->save();
            \Toastr::success('Page created successfully, Page ID ='.$graphNode['id'], $title = null, $options = []);
            return redirect()->route('home');
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            \Toastr::error('Error! Please Try Again.', $title = null, $options = []);
            return redirect()->route('home');
            exit;
        } catch(Facebook\Exceptions\FacebookExceptionsFacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            \Toastr::error('Error! Please Try Again.', $title = null, $options = []);
            return redirect()->route('home');
            exit;
        }
        catch(Exception $e){
            print_r($e->getMessage());
            die();
        }	
    }

    public function page_list() {
        $pages = FbPage::all();
        return view('page_list', compact('pages'));
    }

    public function post($id) {
        $data = FbPage::find($id);
        if (!isset($data->fb_page_token)) {
            $fb = new \Facebook\Facebook([
                    'app_id' => config('app.fb_app_id'),
                    'app_secret' => config('app.fb_app_secret'),	  
                    'default_graph_version' => 'v2.10'
                ]);

            try {
                $response = $fb->get(
                        '/'.$data->fb_page_id.'?fields=access_token',
                        $data->user->fb_token
                    );
                    $graphNode = $response->getGraphNode();
                    $data->fb_page_token = $graphNode['access_token'];
                    $data->save();
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
        }
        return view('create_post',compact('data')); 
    }

    public function make_page(Request $request) {
    	$validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'category' => 'required|string',
            'description' => 'required|string',
            'picture' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            'cover_photo' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
        ],[
            'name.required' => 'name is required',
            'category.required' => 'category is required',
            'description.required' => 'description is required',
            'picture.required' => 'picture is required',
            'cover_photo.required' => 'cover photo is required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                "status" => 400,
                "message" => $validator->errors()->first()
            ], 400);
        }
        if(!$request->fb_user_id ){
            Session::put('fb_user_id', '-1');
            Session::put('params',$request->all());
            return redirect()->route('redirect_api',['service'=>'facebook']);
        }
	$page = FbPage::where('fb_user_id', $request->fb_user_id)->where('name', $request->name);
        if($page->first()){
            $page_name = $request->name.rand(10,100);
        } else {
            $page_name = $request->name;
        }
        $user = User::where('fb_id', $request->fb_user_id)->first();
        $data = new FbPage;
        $data->user_id = $user->id;
        $data->fb_user_id = $request->fb_user_id;
        $data->name = $page_name;
        $data->category = $request->category;
        $data->description = $request->description;
        $data->picture = $request->picture;
        $data->cover_photo = $request->cover_photo;
        $data->save();
        $last_id = $data->id;

        $fb_user_id = $request->fb_user_id;
        $fb_user_token = $user->fb_token;

        $picture = $request->picture;
        $cover_photo = $request->cover_photo;

        try {
            $fb = new \Facebook\Facebook([
                    'app_id' => config('app.fb_app_id'),
                    'app_secret' => config('app.fb_app_secret'),	  
                    'default_graph_version' => 'v2.10'
                ]);

            $data = array (
                    'name' => $page_name,
                    'category_enum' => $request->category,
                    'about' => $request->description,
                    'picture' => $picture,
                    'cover_photo' => '{"url":"'.$cover_photo.'"}'
                );
		   
            $response = $fb->post(
                    '/'.$fb_user_id.'/accounts',
                    $data,
                    $fb_user_token
                );

            $graphNode = $response->getGraphNode();
            $page_data = FbPage::find($last_id);
            $page_data->fb_page_id = $graphNode['id'];
            $page_data->save();

            $data = FbPage::where('fb_page_id', $graphNode['id'])->first();
            if (!isset($data->fb_page_token)) {
                $fb = new \Facebook\Facebook([
                        'app_id' => config('app.fb_app_id'),
                        'app_secret' => config('app.fb_app_secret'),	  
                        'default_graph_version' => 'v2.10'
                        ]);

                try {
                    $response = $fb->get(
                            '/'.$data->fb_page_id.'?fields=access_token',
			    $data->user->fb_token
                        );
                    $graphNode = $response->getGraphNode();
                    $data->fb_page_token = $graphNode['access_token'];
                    $data->save();
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }
            }

            $title = 'Hello Title';
            $text_message = 'This is Foo Video';
            $media_file = config('app.fb_test_video_file');
            $page_data = FbPage::where('fb_page_id', $graphNode['id'])->first();

            $data = new Post;
            $data->page_id = $page_data->id;
            $data->fb_page_id = $page_data->fb_page_id;
            if (isset($title)) {
	        $data->title = $title;
            }
            if (isset($text_message)) {
        	$data->text = $text_message;
            }
            if (isset($media_file)) {
        	$data->file = $media_file;
            }
            $data->save();

            $fb = new \Facebook\Facebook([
                    'app_id' => config('app.fb_app_id'),
                    'app_secret' => config('app.fb_app_secret'),	  
                    'default_graph_version' => 'v2.10'
                ]);

            $image_file_types = array("jpg", "png", "jpeg");
            
            $video_file_types = array("mp4", "3g2", "3gp", "3gpp", "asf", "avi", "dat", "divx", "dv", "f4v", "flv", "gif", "m2ts", "m4v", "mkv", "mod", "mov", "mp4", "mpe", "mpeg", "mpeg4", "mpg", "mts", "nsv", "ogm", "ogv", "qt", "tod", "ts", "vob", "wmv");

            $ext = pathinfo($media_file, PATHINFO_EXTENSION);

            if (isset($media_file)) {
	    	if (in_array($ext, $video_file_types)) {
	    		$post = Post::find($data->id);
				$data = [
			  		'title' => $title,
			  		'description' => $text_message,
                                        'source' => $fb->videoToUpload($media_file),
				];
		    	try {
                            $response1 = $fb->post(
                                '/'.$page_data->fb_page_id.'/videos',
                                $data,
                                $page_data->fb_page_token
                            );
                        } catch(Facebook\Exceptions\FacebookResponseException $e) {
                            echo 'Graph returned an error: ' . $e->getMessage();
                            exit;
                        } catch(Facebook\Exceptions\FacebookSDKException $e) {
                            echo 'Facebook SDK returned an error: ' . $e->getMessage();
                            exit;
                        }
                        $graphNode_post = $response1->getGraphNode();
                        if (isset($graphNode_post['id'])) {
                            $post->fb_post_id = $graphNode_post['id'];
                            $post->save();
                        }
	        }  elseif (in_array($ext, $image_file_types)) {
                    $post = Post::find($data->id);
                    $data = [
                            'message' => $text_message,
                            'url' => $media_file,
                        ];
                    try {
                        $response1 = $fb->post(
                                '/'.$page_data->fb_page_id.'/photos',
                                $data,
                                $page_data->fb_page_token
                        );
                    } catch(Facebook\Exceptions\FacebookResponseException $e) {
                        echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    } catch(Facebook\Exceptions\FacebookSDKException $e) {
                        echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;
                    }
                    $graphNode_post = $response1->getGraphNode();
                    if (isset($graphNode_post['id'])) {
                        $post->fb_post_id = $graphNode_post['id'];
                        $post->save();
                    }
                } else {
                    return response()->json([
                        "status" => 400,
                        "message" => 'Image/Video File is allowed.'
                        ], 400);
                }
            } elseif (isset($text_message)) {
	        $post_data = array (
                            'message' => $text_message,
                );

	        try {
                    $response1 = $fb->post(
                        '/'.$page_data->fb_page_id.'/feed',
                        $post_data,
                        $page_data->fb_page_token
                    );
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                } catch(Exception $e){
                    print_r($e->getMessage());
                    die('hh');
                }
                $graphNode_post = $response1->getGraphNode();
                if (isset($graphNode_post['id'])) {
                    $post = Post::find($data->id);
                    $post->fb_post_id = $graphNode_post['id'];
                    $post->save();
                }
            } 
        return redirect('https://www.facebook.com/'.str_replace(' ', '-', $page_name).'-'.$graphNode['id']);
		
        return response()->json([
            "status" => 200,
            "message" => 'Post is created, Post Id = '. $graphNode_post['id']
        ], 200);
        echo 'Post created successfully, Post ID = '.$graphNode_post['id'];

            return response()->json([
                "status" => 200,
                "message" => 'Page is created, Page Id = '. $graphNode['id']
            ], 200);
          echo 'Page is created, Page Id = '. $graphNode['id'];
          // dd($graphNode);
          die;
		}
        catch(Exception $e){
            return response()->json([
                "status" => 400,
                "message" => $e->getMessage(),
                "code"  => $e->getCode()
            ], 400);
        }	
    }
}
