<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Kamaln7\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Redirect;
use Session;

class SocialAuthController extends Controller {

    public function __construct() {
        
    }

    public function redirect($service) {
        return Socialite::driver($service)->redirect();
    }

    public function api_redirect($service) {
        return Socialite::driver($service)->redirectUrl(config('app.fb_app_api_redirect'))->redirect();
    }

    public function handleProviderCallback($provider) {
        try {
            $data = Socialite::driver($provider)->user();
            $user = User::updateOrCreate(
                            ['email' => $data->email], ['name' => $data->name, 'email' => $data->email, 'fb_id' => $data->id, 'fb_name' => $data->name, 'fb_email' => $data->email, 'fb_token' => $data->token]
            );
            \Toastr::success('Facebook Account Sync Successfully!', $title = null, $options = []);
            return redirect()->route('page', ['id' => $user->id]);
        } catch (\Exception $e) {
            \Toastr::error('Error! Please Try Again.', $title = null, $options = []);
            return redirect()->route('home');
        }
    }

    public function api_handleProviderCallback($provider) {

        try {
            $data = Socialite::driver($provider)->redirectUrl(config('app.fb_app_api_redirect'))->user();
            $user = User::updateOrCreate(
                            ['email' => $data->email], ['name' => $data->name, 'email' => $data->email, 'fb_id' => $data->id, 'fb_name' => $data->name, 'fb_email' => $data->email, 'fb_token' => $data->token]
            );
            $fb_user_id = Session::get('fb_user_id');
            if ($fb_user_id == '-1') {
                $params = Session::get('params');
                $params['fb_user_id'] = $data->id;
                return redirect()->route('make_page', $params);
            } else
                return response()->json([
                            "status" => 200,
                            "message" => 'Successfully logged in with Facebook',
                            "facebook_id" => $data->id
                                ], 400);
        } catch (\Exception $e) {
            return response()->json([
                        "status" => 400,
                        "message" => 'Error, Please try again.'
                            ], 400);
        }
    }

}
