<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\FbPage;
use App\Post;
use Kamaln7\Toastr\Facades\Toastr;
use Validator;

class PostController extends Controller {

    public function __construct() {
        
    }

    public function index() {
        $posts = Post::all();
        return view('post_list', compact('posts'));
    }

    public function store(Request $request) {
        $page_data = FbPage::find($request->page_id);

        if ($request->hasfile('media_file')) {
            $file_name = time() . rand(10000, 99999) . $request->file('media_file')->getClientOriginalName();
            $request->file('media_file')->move(public_path('fb_post_files'), $file_name);
        }

        $data = new Post;
        $data->page_id = $request->page_id;
        $data->fb_page_id = $page_data->fb_page_id;
        if (isset($request->title)) {
            $data->title = $request->title;
        }
        if (isset($request->text_message)) {
            $data->text = $request->text_message;
        }
        if (isset($file_name)) {
            $data->file = $file_name;
        }
        $data->save();

        $fb = new \Facebook\Facebook([
            'app_id' => config('app.fb_app_id'),
            'app_secret' => config('app.fb_app_secret'),
            'default_graph_version' => 'v2.10'
        ]);

        $image_file_types = array("jpg", "png", "jpeg");

        $video_file_types = array("mp4", "3g2", "3gp", "3gpp", "asf", "avi", "dat", "divx", "dv", "f4v", "flv", "gif", "m2ts", "m4v", "mkv", "mod", "mov", "mp4", "mpe", "mpeg", "mpeg4", "mpg", "mts", "nsv", "ogm", "ogv", "qt", "tod", "ts", "vob", "wmv");
        if (isset($file_name)) {
            if (in_array($request->file('media_file')->getClientOriginalExtension(), $video_file_types)) {
                $post = Post::find($data->id);
                $url = config('app.fb_post_files_base_path');
                $video_file = $url . $post->file;
                $data = [
                    'title' => $request->title,
                    'description' => $request->text_message,
                    'source' => $fb->videoToUpload($video_file),
                ];
                try {
                    $response = $fb->post(
                            '/' . $page_data->fb_page_id . '/videos', $data, $page_data->fb_page_token
                    );
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }
                $graphNode = $response->getGraphNode();
                if (isset($graphNode['id'])) {
                    $post->fb_post_id = $graphNode['id'];
                    $post->save();
                }
            } elseif (in_array($request->file('media_file')->getClientOriginalExtension(), $image_file_types)) {
                $post = Post::find($data->id);
                $url = config('app.fb_post_files_base_path');
                $picture = $url . $post->file;
                $data = [
                    'message' => $request->text_message,
                    'url' => $picture,
                ];
                try {
                    $response = $fb->post(
                            '/' . $page_data->fb_page_id . '/photos', $data, $page_data->fb_page_token
                    );
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }
                $graphNode = $response->getGraphNode();
                if (isset($graphNode['id'])) {

                    $post->fb_post_id = $graphNode['id'];
                    $post->save();
                }
            }
        } elseif (isset($request->text_message)) {
            $post_data = array(
                'message' => $request->text_message,
            );

            try {
                $response = $fb->post(
                        '/' . $page_data->fb_page_id . '/feed', $post_data, $page_data->fb_page_token
                );
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            } catch (Exception $e) {
                print_r($e->getMessage());
                die('hh');
            }
            $graphNode = $response->getGraphNode();
            if (isset($graphNode['id'])) {
                $post = Post::find($data->id);
                $post->fb_post_id = $graphNode['id'];
                $post->save();
            }
        } else {
            \Toastr::error('Empty Post cannot be created, Please Try Again', $title = null, $options = []);
            return redirect()->route('page_list');
        }
        \Toastr::success('Post created successfully, Post ID =' . $graphNode['id'], $title = null, $options = []);
        return redirect()->route('page_list');
    }

    public function make_post(Request $request) {

        $validator = Validator::make($request->all(), [
                    'page_id' => 'required|numeric',
                    'title' => 'string',
                    'text_message' => 'string',
                    'media_file' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                        ], [
                    'page_id.required' => 'Page ID is required',
                    'page_id.numeric' => 'Page ID must be a number',
                    'title.string' => 'Title must be string.',
                    'text_message.string' => 'Message must be string.',
                    'media_file.string' => 'Description must be string.'
        ]);
        if ($validator->fails()) {
            return response()->json([
                        "status" => 400,
                        "message" => $validator->errors()->first()
                            ], 400);
        }
        $data = FbPage::where('fb_page_id', $request->page_id)->first();
        if (!isset($data->fb_page_token)) {
            $fb = new \Facebook\Facebook([
                'app_id' => config('app.fb_app_id'),
                'app_secret' => config('app.fb_app_secret'),
                'default_graph_version' => 'v2.10'
            ]);

            try {
                $response = $fb->get(
                        '/' . $data->fb_page_id . '?fields=access_token', $data->user->fb_token
                );
                $graphNode = $response->getGraphNode();
                $data->fb_page_token = $graphNode['access_token'];
                $data->save();
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }
        }



        $page_data = FbPage::where('fb_page_id', $request->page_id)->first();

        $data = new Post;
        $data->page_id = $page_data->id;
        $data->fb_page_id = $page_data->fb_page_id;
        if (isset($request->title)) {
            $data->title = $request->title;
        }
        if (isset($request->text_message)) {
            $data->text = $request->text_message;
        }
        if (isset($request->media_file)) {
            $data->file = $request->media_file;
        }
        $data->save();

        $fb = new \Facebook\Facebook([
            'app_id' => config('app.fb_app_id'),
            'app_secret' => config('app.fb_app_secret'),
            'default_graph_version' => 'v2.10'
        ]);

        $image_file_types = array("jpg", "png", "jpeg");

        $video_file_types = array("mp4", "3g2", "3gp", "3gpp", "asf", "avi", "dat", "divx", "dv", "f4v", "flv", "gif", "m2ts", "m4v", "mkv", "mod", "mov", "mp4", "mpe", "mpeg", "mpeg4", "mpg", "mts", "nsv", "ogm", "ogv", "qt", "tod", "ts", "vob", "wmv");

        $ext = pathinfo($request->media_file, PATHINFO_EXTENSION);

        if (isset($request->media_file)) {
            if (in_array($ext, $video_file_types)) {
                $post = Post::find($data->id);
                $data = [
                    'title' => $request->title,
                    'description' => $request->text_message,
                    'source' => $fb->videoToUpload($request->media_file),
                ];
                try {
                    $response = $fb->post(
                            '/' . $page_data->fb_page_id . '/videos', $data, $page_data->fb_page_token
                    );
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }
                $graphNode = $response->getGraphNode();
                if (isset($graphNode['id'])) {
                    $post->fb_post_id = $graphNode['id'];
                    $post->save();
                }
            } elseif (in_array($ext, $image_file_types)) {
                $post = Post::find($data->id);
                $data = [
                    'message' => $request->text_message,
                    'url' => $request->media_file,
                ];
                try {
                    $response = $fb->post(
                            '/' . $page_data->fb_page_id . '/photos', $data, $page_data->fb_page_token
                    );
                } catch (Facebook\Exceptions\FacebookResponseException $e) {
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch (Facebook\Exceptions\FacebookSDKException $e) {
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }
                $graphNode = $response->getGraphNode();
                if (isset($graphNode['id'])) {

                    $post->fb_post_id = $graphNode['id'];
                    $post->save();
                }
            } else {
                return response()->json([
                            "status" => 400,
                            "message" => 'Image/Video File is allowed.'
                                ], 400);
            }
        } elseif (isset($request->text_message)) {
            $post_data = array(
                'message' => $request->text_message,
            );

            try {
                $response = $fb->post(
                        '/' . $page_data->fb_page_id . '/feed', $post_data, $page_data->fb_page_token
                );
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            } catch (Exception $e) {
                print_r($e->getMessage());
                die('hh');
            }
            $graphNode = $response->getGraphNode();
            if (isset($graphNode['id'])) {
                $post = Post::find($data->id);
                $post->fb_post_id = $graphNode['id'];
                $post->save();
            }
        } else {
            \Toastr::error('Empty Post cannot be created, Please Try Again', $title = null, $options = []);
            return redirect()->route('page_list');
        }
        echo 'Post created successfully, Post ID = ' . $graphNode['id'];
        die;
    }

}
