<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'fb_posts';


    public function page(){
    	return $this->BelongsTo('App\FbPage','page_id');
    }
}
