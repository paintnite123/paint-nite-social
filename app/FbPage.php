<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FbPage extends Model
{
    protected $table = 'fb_pages';

    public function user(){
    	return $this->BelongsTo('App\User','user_id');
    }

    public function posts()
    {
        return $this->hasMany('App\Post' , 'page_id');
    }
}
