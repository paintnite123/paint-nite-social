<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//Route::get('/', 'HomeController@index')->name("home");

Route::get('/redirect/{service}', 'SocialAuthController@redirect');
Route::get('/callback/{service}', 'SocialAuthController@handleProviderCallback');
Route::get('/redirect/api/{service}', 'SocialAuthController@api_redirect')->name('redirect_api');
Route::get('/callback/api/{service}', 'SocialAuthController@api_handleProviderCallback');

Route::get('/', 'UserController@index')->name("home");

Route::get('/page/{id}', 'PageController@index')->name("page");
Route::post('/store', 'PageController@store')->name("store_page");
Route::get('/create/{id}/{user_id}', 'PageController@create')->name("create_page");
Route::get('/list', 'PageController@page_list')->name("page_list");

Route::get('/post/{id}', 'PageController@post')->name("post");
Route::post('/store_post', 'PostController@store')->name("store_post");
Route::get('/post_list', 'PostController@index')->name("post_list");

Route::get('/posting', 'PostController@posting')->name("posting");

Route::get('/make_page', 'PageController@make_page')->name("make_page");
Route::get('/make_post', 'PostController@make_post')->name("make_post");